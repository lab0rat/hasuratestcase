## Getting Started

To run this project:

Clone this repo
Then type

cd hasuratestcase
yarn 
yarn dev


## Config variables

You can specify your config vars in /src/config.ts

API_URL - graphQL endpoint of your Hasura API
DATA_URL - URL of JSON data file
HASURA_ADMIN_SECRET - secret key to your Hasura API